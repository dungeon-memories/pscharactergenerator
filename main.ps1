Class People {
    [String]$name
    [String]$race
    [bool]$gender
    [int]$age
    [int]$weight
    [int]$height
    [String]$hait_color
    [String]$hair_length
    [String]$appearance 
    [String]$ability_high 
    [String]$ability_low
    [String]$talent
    [String]$mannerism
    [String]$trait
    [String]$ideal
    [String]$bond
    [String]$flaw
    [int]$priority
    [bool]$alive
}

$attributes = @("appearances", "bonds", "flaws", "high_ability", "ideals", "low_ability", "manners", "names", "races", "talents", "traits")
$data = @{}
foreach($type in $attributes) {
    $data[$type] = (Get-Content ".\$type.json") -join "`n" | ConvertFrom-Json
}

$person = New-Object People
$person.race = $data.races[(Get-Random -Maximum ($data.races).count)]._name
$gender = @("Female", "Male")
$pgender = $gender[(Get-Random -Maximum $gender.Count)]
$names = $data.names.($person.race)
$name = ""
if ($names.$pgender) {
    $name += $names.$pgender[(Get-Random -Maximum ($names.$pgender).Count)] + " "
}
if ($names.Nickname) {
    $name += "'" + $names.Nickname[(Get-Random -Maximum ($names.Nickname).Count)] + "' "
}
if ($names.Family) {
    $name += $names.Family[(Get-Random -Maximum ($names.Family).Count)]
}
$name
# $person | ConvertTo-Json | Write-Host